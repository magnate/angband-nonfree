# angband-nonfree

## Description
This is the Debian package for the non-free components of the game Angband.

## Installation
Designed to be installed by apt:
 apt install angband-nonfree

## Authors and acknowledgment
## License
Please see debian/copyright for license info
